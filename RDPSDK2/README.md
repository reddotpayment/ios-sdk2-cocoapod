# RDPSDK2

[![Version](https://img.shields.io/cocoapods/v/RDPSDK2.svg?style=flat)](http://cocoapods.org/pods/RDPSDK2)
[![License](https://img.shields.io/cocoapods/l/RDPSDK2.svg?style=flat)](http://cocoapods.org/pods/RDPSDK2)
[![Platform](https://img.shields.io/cocoapods/p/RDPSDK2.svg?style=flat)](http://cocoapods.org/pods/RDPSDK2)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RDPSDK2 is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RDPSDK2'
```

## Author

Daryl, darylng@reddotpayment.com

## License

RDPSDK2 is available under the MIT license. See the LICENSE file for more info.
