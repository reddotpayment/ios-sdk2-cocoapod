#
# Be sure to run `pod lib lint RDPSDK2.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RDPSDK2'
  s.version          = '2.0.1'
  s.summary          = 'This RDPSDK2 is the RDP mobile SDK version 2.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'RDP SDK 2 for merchants to integrate RDP payment solutions with their iOS projects.'

  s.homepage         = 'https://bitbucket.org/reddotpayment/ios-sdk2-cocoapod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Daryl' => 'darylng@reddotpayment.com' }
  s.source           = { :git => 'https://darylngrdp@bitbucket.org/reddotpayment/ios-sdk2-cocoapod.git', :tag => s.version }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'RDPSDK2/RDPSDK2/Classes/**/*'
  
  # s.resource_bundles = {
  #   'RDPSDK2' => ['RDPSDK2/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'CardIO'
end
